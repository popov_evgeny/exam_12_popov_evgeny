const mongoose = require('mongoose');
const config = require("./config");
const User = require("./models/User");
const {nanoid} = require("nanoid");
const Photo = require("./models/Photo");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [Jack, John] = await User.create({
    email: 'test@test.com',
    password: '123',
    name: 'Jack Doe',
    token: nanoid(),
  },{
    email: 'test2@test.com',
    password: '123',
    name: 'John Doe',
    token: nanoid(),
  });

  await Photo.create({
    title: 'picture1',
    user: Jack,
    image: 'pic1.jpg'
  },{
    title: 'picture2',
    user: Jack,
    image: 'pic2.jpg'
  },{
    title: 'picture3',
    user: Jack,
    image: 'pic3.jpg'
  },{
    title: 'picture4',
    user: Jack,
    image: 'pic4.jpeg'
  },{
    title: 'picture5',
    user: Jack,
    image: 'pic5.jpg'
  },{
    title: 'picture6',
    user: John,
    image: 'pic6.jpg'
  },{
    title: 'picture7',
    user: John,
    image: 'pic7.jpeg'
  },{
    title: 'picture8',
    user: John,
    image: 'pic8.jpg'
  },{
    title: 'picture9',
    user: John,
    image: 'pic9.jpg'
  },{
    title: 'picture10',
    user: John,
    image: 'pic10.jpeg'
  },)

  await mongoose.connection.close();
};

run().catch(e => console.error(e));