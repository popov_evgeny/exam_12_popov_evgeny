const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadsPath : path.join(rootPath, 'public/uploads'),
  mongo : {
    db: 'mongodb://localhost/gallery',
    options: {useNewUrlParser: true},
  },
  facebook: {
    appId: '534960134813463',
    appSecret: '48b997c0ac67103ad759e0ac29627592'
  }
}