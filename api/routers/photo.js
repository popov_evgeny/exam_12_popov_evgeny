const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const path = require('path');
const config = require('../config');
const Photo = require('../models/Photo');
const auth = require("../middleware/auth");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req,file,cd) => {
    cd(null, config.uploadsPath);
  },
  filename: (req, file, cd) => {
    cd(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req , res) => {
  let query = {};
  if (req.query.user) {
    query.user = {_id: req.query.user}
  }
  const photos = await Photo.find(query).populate('user', 'name');
  res.send(photos);
});

router.get('/:id', async (req , res, next) => {
  try {
    const photo = await Photo.findOne({_id: req.params.id});

    return res.send(photo);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, upload.single('image'), async (req , res, next) => {
  try {

    const photoData = {
      title: req.body.title,
      user: req.user._id,
      image: null
    }

    if (req.file){
      photoData.image = req.file.filename;
    }

    const photo = new Photo(photoData);
    await photo.save();

    return res.send({message: 'Added new photo in database'});
  } catch (e) {
    next(e);
  }
});


router.delete('/:id', async (req , res, next) => {
  try {
    await Photo.deleteOne({_id: req.params.id});

    return res.send({message: 'Photo delete!!'});
  } catch (e) {
    next(e);
  }
});

module.exports = router;