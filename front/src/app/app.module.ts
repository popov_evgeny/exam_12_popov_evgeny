import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { localStorageSync } from 'ngrx-store-localstorage';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { environment } from '../environments/environment';
import { userReducer } from './store/users.reducer';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { MatMenuModule } from '@angular/material/menu';
import { UserTypeDirective } from './directives/user-type.directive';
import { EffectsModule } from '@ngrx/effects';
import { UsersEffects } from './store/users.effects';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { LoginFormComponent } from './pages/login/login-form.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { AuthInterceptor } from './auth.interceptor';
import { RegisterComponent } from './pages/register/register.component';
import { ValidatePasswordDirective } from './validate-password.directive';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { HomeComponent } from './pages/home/home.component';
import { photoReducer } from './store/photo.reducer';
import { PhotoEffects } from './store/photo.effects';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MyGalleryComponent } from './pages/my-gallery/my-gallery.component';
import { AddPhotoComponent } from './pages/add-photo/add-photo.component';
import { DialogComponent } from './ui/dialog/dialog.component';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { NotFoundComponent } from './not-found.component';

export const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{'users': ['user']}],
    rehydrate: true
  })(reducer);
}

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(environment.fbAppId, {
        scope: 'email,public_profile'
      })
    }
  ]
}

const metaReducers: Array<MetaReducer> = [localStorageSyncReducer];

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    UserTypeDirective,
    LoginFormComponent,
    RegisterComponent,
    ValidatePasswordDirective,
    FileInputComponent,
    HomeComponent,
    MyGalleryComponent,
    AddPhotoComponent,
    DialogComponent,
    NotFoundComponent
  ],
  entryComponents: [DialogComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    HttpClientModule,
    MatSnackBarModule,
    FlexLayoutModule,
    FormsModule,
    StoreModule.forRoot({
      users: userReducer,
      photos: photoReducer,
    }, {metaReducers}),
    EffectsModule.forRoot([UsersEffects, PhotoEffects]),
    MatMenuModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatInputModule,
    SocialLoginModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatDialogModule

  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: 'SocialAuthServiceConfig', useValue: socialConfig},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
