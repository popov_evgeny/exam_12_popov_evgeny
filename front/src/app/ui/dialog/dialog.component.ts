import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Photo } from '../../models/photo.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.sass']
})
export class DialogComponent implements OnInit, OnDestroy {
  photo: Observable<null | Photo>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  photoSub!: Subscription;
  api = environment.apiUrl;
  photoData!: Photo;

  constructor(private store: Store<AppState>) {
    this.photo = this.store.select(state => state.photos.photo);
    this.loading = this.store.select( state => state.photos.fetchOneLoading);
    this.error = this.store.select( state => state.photos.fetchOneError);
  }

  ngOnInit(): void {
    this.photoSub = this.photo.subscribe( photo => {
      this.photoData = <Photo>photo;
    })
  }

  ngOnDestroy() {
    this.photoSub.unsubscribe();
  }

}
