import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ActivatedRoute } from '@angular/router';
import { fetchDialogPhotoRequest, fetchUserPhotoRequest, removePhotoRequest } from '../../store/photo.actions';
import { Observable, Subscription } from 'rxjs';
import { Photo } from '../../models/photo.model';
import { environment } from '../../../environments/environment';
import { User } from '../../models/user.model';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../../ui/dialog/dialog.component';

@Component({
  selector: 'app-my-gallery',
  templateUrl: './my-gallery.component.html',
  styleUrls: ['./my-gallery.component.sass']
})
export class MyGalleryComponent implements OnInit, OnDestroy {
  user: Observable<null | User>;
  userSub!: Subscription;
  photos: Observable<Photo[]>;
  photoSub!: Subscription;
  loading: Observable<boolean>;
  removeLoading: Observable<boolean>;
  error: Observable<null | string>;
  api = environment.apiUrl;
  userId!: string;
  photoUserId!: string;
  photoUserName!: string;
  photoId = '';

  constructor(private store: Store<AppState>, private route: ActivatedRoute, public dialog: MatDialog) {
    this.user = store.select(state => state.users.user);
    this.photos = this.store.select(state => state.photos.photos);
    this.loading = this.store.select( state => state.photos.fetchLoading);
    this.error = this.store.select( state => state.photos.fetchError);
    this.removeLoading = this.store.select( state => state.photos.removeLoading);
  }

  ngOnInit(): void {
    this.route.params.subscribe( params => {
      this.photoUserId = params['id'];
      this.store.dispatch(fetchUserPhotoRequest({id: params['id']}));
    });
    this.photoSub = this.photos.subscribe( photos => {
      this.photoUserName = photos[0]?.user.name;
    });
    this.userSub = this.user.subscribe( user => {
      this.userId = user?._id!;
    });
  }

  openDialog(id: string) {
    this.store.dispatch(fetchDialogPhotoRequest({id}))
    this.dialog.open(DialogComponent);
  }

  onRemovePhoto(id: string, event: Event) {
    event.stopImmediatePropagation();
    this.photoId = id;
    this.store.dispatch(removePhotoRequest({id}));
  }

  ngOnDestroy() {
    this.photoSub.unsubscribe();
    this.userSub.unsubscribe();
  }
}
