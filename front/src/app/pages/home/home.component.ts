import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Observable } from 'rxjs';
import { Photo } from '../../models/photo.model';
import { environment } from '../../../environments/environment';
import { fetchDialogPhotoRequest, fetchPhotosRequest } from '../../store/photo.actions';
import { DialogComponent } from '../../ui/dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  photos: Observable<Photo[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  api = environment.apiUrl;


  constructor(private store: Store<AppState>, public dialog: MatDialog) {
    this.photos = this.store.select(state => state.photos.photos);
    this.loading = this.store.select( state => state.photos.fetchLoading);
    this.error = this.store.select( state => state.photos.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchPhotosRequest());
  }

  openDialog(id: string) {
    this.store.dispatch(fetchDialogPhotoRequest({id}))
    this.dialog.open(DialogComponent);
  }

}
