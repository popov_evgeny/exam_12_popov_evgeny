import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ApiPhoto } from '../../models/photo.model';
import { createPhotoRequest } from '../../store/photo.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-photo',
  templateUrl: './add-photo.component.html',
  styleUrls: ['./add-photo.component.sass']
})
export class AddPhotoComponent implements OnInit {
  @ViewChild('form') form!: NgForm;
  loading: Observable<boolean>;
  createError: Observable<null | string>;

  constructor(  private store: Store<AppState>) {
    this.loading = this.store.select( state => state.photos.createLoading);
    this.createError = this.store.select( state => state.photos.createError);
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const photoData: ApiPhoto = this.form.value;
    this.store.dispatch(createPhotoRequest({photoData}))
  }
}
