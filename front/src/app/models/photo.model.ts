export class Photo {
  constructor(
    public _id: string,
    public title: string,
    public user: {
      _id: string,
      name: string
    },
    public image: string
  ) {}
}

export interface ApiPhoto {
  [key: string]: any,
  title: string,
  user: {
    _id: string,
    name: string
  },
  image: string
}
