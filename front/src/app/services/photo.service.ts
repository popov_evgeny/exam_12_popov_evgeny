import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiPhoto, Photo } from '../models/photo.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor(private http: HttpClient) { }

  getPhotos() {
    return this.http.get<Photo[]>(environment.apiUrl + '/photo').pipe(map(response => {
      return response.map( photoData => {
        return new Photo(
          photoData._id,
          photoData.title,
          photoData.user,
          photoData.image
        )});
    }));
  }

  getDialogPhoto(id: string) {
    return this.http.get<Photo>(environment.apiUrl + '/photo/' + id);
  }

  getUserPhotos(id: string) {
    return this.http.get<Photo[]>(environment.apiUrl + '/photo?user=' + id).pipe(map(response => {
      return response.map(photoData => {
        return new Photo(
          photoData._id,
          photoData.title,
          photoData.user,
          photoData.image
        )
      });
    }))
  }

  createPhoto(photoData: ApiPhoto) {

    const formData = new FormData();
    formData.append('title', photoData.title);

    if (photoData.image) {
      formData.append('image', photoData.image);
    }

    return this.http.post(environment.apiUrl + '/photo', formData);
  }


  removePhoto(id: string) {
    return this.http.delete(environment.apiUrl + '/photo/' + id);
  }
}
