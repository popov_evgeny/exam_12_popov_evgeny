import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginFormComponent } from './pages/login/login-form.component';
import { RegisterComponent } from './pages/register/register.component';
import { HomeComponent } from './pages/home/home.component';
import { MyGalleryComponent } from './pages/my-gallery/my-gallery.component';
import { AddPhotoComponent } from './pages/add-photo/add-photo.component';
import { NotFoundComponent } from './not-found.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'login', component: LoginFormComponent},
  { path: 'register', component: RegisterComponent},
  { path: ':id/my_gallery', component: MyGalleryComponent},
  { path: 'create-new-photo', component: AddPhotoComponent},
  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
