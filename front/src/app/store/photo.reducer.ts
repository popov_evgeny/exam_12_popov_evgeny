import { PhotoState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createPhotoFailure,
  createPhotoRequest, createPhotoSuccess, fetchDialogPhotoFailure, fetchDialogPhotoRequest, fetchDialogPhotoSuccess,
  fetchPhotosFailure,
  fetchPhotosRequest,
  fetchPhotosSuccess, fetchUserPhotoFailure,
  fetchUserPhotoRequest,
  fetchUserPhotoSuccess, removePhotoRequest
} from './photo.actions';

const initialState: PhotoState = {
  photos: [],
  photo: null,
  fetchLoading: false,
  fetchError: null,
  fetchOneLoading: false,
  fetchOneError: null,
  createLoading: false,
  createError: null,
  removeLoading: false,
};

export  const photoReducer = createReducer(
  initialState,

  on(fetchPhotosRequest, state => ({...state, fetchLoading: true, fetchError: null})),
  on(fetchPhotosSuccess, (state, {photos}) => ({...state, fetchLoading: false, photos})),
  on(fetchPhotosFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchDialogPhotoRequest, state => ({...state, fetchOneLoading: true, fetchOneError: null})),
  on(fetchDialogPhotoSuccess, (state, {photo}) => ({...state, fetchOneLoading: false, photo})),
  on(fetchDialogPhotoFailure, (state, {error}) => ({...state, fetchOneLoading: false, fetchOneError: error})),

  on(fetchUserPhotoRequest, state => ({...state, fetchLoading: true, fetchError: null})),
  on(fetchUserPhotoSuccess, (state, {photos}) => ({...state, fetchLoading: false, photos})),
  on(fetchUserPhotoFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createPhotoRequest, state => ({...state, createLoading: true})),
  on(createPhotoSuccess, state => ({...state, createLoading: false})),
  on(createPhotoFailure, (state, {error}) => ({...state, createLoading: false, createError: error,})),

  on(removePhotoRequest, (state, {id}) => {
    const updatePhotos = state.photos.filter( photo => {
      return photo._id !== id;
    });

    return {...state, photos: updatePhotos, removeLoading: true}
  }),
  on(createPhotoSuccess, state => ({...state, removeLoading: false})),
)
