import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { map, mergeMap, tap } from 'rxjs';
import { HelpersService } from '../services/helpers.service';
import {
  createPhotoFailure,
  createPhotoRequest, createPhotoSuccess, fetchDialogPhotoFailure, fetchDialogPhotoRequest, fetchDialogPhotoSuccess,
  fetchPhotosFailure,
  fetchPhotosRequest,
  fetchPhotosSuccess, fetchUserPhotoFailure,
  fetchUserPhotoRequest,
  fetchUserPhotoSuccess, removePhotoRequest, removePhotoSuccess
} from './photo.actions';
import { PhotoService } from '../services/photo.service';

@Injectable()

export class PhotoEffects {
  constructor(
    private actions: Actions,
    private photoService: PhotoService,
    private router: Router,
    private helpers: HelpersService,
  ) {
  }

  fetchPhoto = createEffect(() => this.actions.pipe(
    ofType(fetchPhotosRequest),
    mergeMap(() => this.photoService.getPhotos().pipe(
      map(photos => fetchPhotosSuccess({photos})),
      this.helpers.catchServerError(fetchPhotosFailure)
    ))));

  fetchDialogPhoto = createEffect(() => this.actions.pipe(
    ofType(fetchDialogPhotoRequest),
    mergeMap(({id}) => this.photoService.getDialogPhoto(id).pipe(
      map(photo => fetchDialogPhotoSuccess({photo})),
      this.helpers.catchServerError(fetchDialogPhotoFailure)
    ))));

  fetchUserPhotos = createEffect(() => this.actions.pipe(
    ofType(fetchUserPhotoRequest),
    mergeMap(({id}) => this.photoService.getUserPhotos(id).pipe(
      map(photos => fetchUserPhotoSuccess({photos})),
      this.helpers.catchServerError(fetchUserPhotoFailure)
    ))));

  createPhoto = createEffect(() => this.actions.pipe(
    ofType(createPhotoRequest),
    mergeMap(({photoData}) => this.photoService.createPhoto(photoData).pipe(
      map(() => createPhotoSuccess()),
      tap(() => this.router.navigate(['/'])),
      this.helpers.catchServerError(createPhotoFailure)
    ))
  ));

  removeTrack = createEffect(() => this.actions.pipe(
    ofType(removePhotoRequest),
    mergeMap(({id}) => this.photoService.removePhoto(id).pipe(
      map(() => removePhotoSuccess())
    ))
  ));

}

