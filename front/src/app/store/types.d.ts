import { LoginError, RegisterError, User } from '../models/user.model';
import { Photo } from '../models/photo.model';

export type UserState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loadingFb: boolean,
  loginError: null | LoginError
};

export type PhotoState = {
  photos: Photo[],
  photo: null | Photo,
  fetchLoading: boolean,
  fetchError: null | string,
  fetchOneLoading: boolean,
  fetchOneError: null | string,
  createLoading: boolean,
  createError: null | string,
  removeLoading: boolean,
};

export type AppState = {
  users: UserState,
  photos: PhotoState
}
