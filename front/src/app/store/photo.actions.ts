import { createAction, props } from '@ngrx/store';
import { ApiPhoto, Photo } from '../models/photo.model';

export const fetchPhotosRequest = createAction('[Photo] Fetch Photos Request');
export const fetchPhotosSuccess = createAction('[Photo] Fetch Photos Success', props<{photos: Photo[]}>());
export const fetchPhotosFailure = createAction('[Photo] Fetch Photos Failure', props<{error: null | string}>());

export const fetchDialogPhotoRequest = createAction('[Photo] Fetch Dialog Photo Request', props<{id: string}>());
export const fetchDialogPhotoSuccess = createAction('[Photo] Fetch Dialog Photo Success', props<{photo: Photo}>());
export const fetchDialogPhotoFailure = createAction('[Photo] Fetch Dialog Photo Failure', props<{error: null | string}>());

export const fetchUserPhotoRequest = createAction('[Photo] Fetch User Photos Request', props<{id: string}>());
export const fetchUserPhotoSuccess = createAction('[Photo] Fetch User Photos Success', props<{photos: Photo[]}>());
export const fetchUserPhotoFailure = createAction('[Photo] Fetch User Photos Failure', props<{error: null | string}>());

export const createPhotoRequest = createAction('[Photo] Create Request', props<{photoData: ApiPhoto}>());
export const createPhotoSuccess = createAction('[Photo] Create Success');
export const createPhotoFailure = createAction('[Photo] Create Failure', props<{error: string}>());


export const removePhotoRequest = createAction('[Photo] Remove Request', props<{id: string}>());
export const removePhotoSuccess = createAction('[Photo] Remove Success');
